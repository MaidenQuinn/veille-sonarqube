# Veille SonarQube

## Liens utiles: 
- [Documentation SonarQube](https://docs.sonarqube.org/latest/)
- [Explication wikipedia](https://fr.wikipedia.org/wiki/SonarQube)
- [Présentation google slide](https://docs.google.com/presentation/d/1pl6wLGL_OsD30gazXAua7nmd3-aX1WsPW_rtnw-94-A/edit?usp=sharing)
- [Configurer SonarQube sur un projet Angular](https://medium.com/@learning.bikash/angular-code-coverage-with-sonarqube-d2283442080b)


## Installation de SonarQube en local :

Suivre les informations disponibles sur la documentation officielle
https://docs.sonarqube.org/latest/setup/get-started-2-minutes/

Notes : 
- SonarQube ne supporte pas les versions de java supérieures à Java 15. Un projet en java 17 ne pourra pas être analysé par l'outil.
- L'installation demande de placer le dossier sonarQube dans le dossier /opt du système, ne pas oublier de vous attribuer les droits nécessaire pour pouvoir écrire à l'intérieur du dossier.
